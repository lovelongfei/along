import time
def time_in():
    t = time.time()
    print('加密后的当前时间,在d:/time.txt文件查看：',t)
    ti = open('d:/time.txt', 'w')
    ti.writelines(str(t))
    ti.close()
    t = time.localtime(t)  # 时间戳转化成格式化时间时间元组。
    t = time.strftime("%Y-%m-%d %H:%M:%S", t)  # 将时间元组转化成定制时间格式
    print('当前的时间是：',t)

#获取当前的时间戳
def time_out():
    print('输入要查看的加密时间:')
    while True:
        t1 = input()
        if t1 == '3':
            print('退出查询')
            break
        if len(t1) == 17 or len(t1) == 18:
                t1 = float(t1)
                t1 = time.localtime(t1)  # 时间戳转化成格式化时间时间元组。
                t1 = time.strftime("%Y-%m-%d %H:%M:%S", t1)  # 将时间元组转化成定制时间格式
                print(t1)
        else:
            print('请输入正确数字：')
if __name__ == "__main__":
    print('1是输出当前加密时间，2是查看加密时间，3是退出')
    while True:
        aa = input()
        if aa == '1':
            time_in()
        elif aa == '2':
            time_out()
        elif aa == '3':
            print('退出程序')
            exit()
        else:
            print('1是输出当前加密时间，2是查看加密时间，3是退出')
#pyinstaller -i logo.ico -F time_jm.py 转换成exe
